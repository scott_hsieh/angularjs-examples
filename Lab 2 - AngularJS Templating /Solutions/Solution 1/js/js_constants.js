
var evaluations = [
  {
    "evaluation_id": 1,
    "type": "contributing",
    "status": "completed",
    "evaluator_is_anonymous": true,
    "evaluator_role": {
      "id": 1,
      "name": "student"
    },
    "evaluator": {
      "person_id": null,
      "firstname": "anonymous",
      "middlename": "anonymous",
      "lastname": "anonymous",
      "email": "anonymous",
      "role": null,
      "url": null
    },
    "target_role": {
      "id": -1,
      "name": "event"
    },
    "target": {
      "event_id": 9518,
      "category": "rotation",
      "name": "NEUROLOGY CLERKSHIP",
      "abbrev": "NEUR 701",
      "starts_on": "2012-06-18T00:00:00-0700",
      "ends_on": "2012-07-16T23:59:59-0700",
      "all_day_event": true,
      "location": {
        "site": null,
        "building": null,
        "room": null
      },
      "url": "/api/v1/events/rotation/9518",
      "block": {
        "starts_on": "2012-06-18T00:00:00-0700",
        "ends_on": "2012-07-15T23:59:59-0700",
        "id": 284,
        "active": true
      }
    },
    "head_evaluator": null,
    "completed_on": "2012-07-23T00:00:00-0700",
    "received_on": "2012-07-23T00:00:00-0700",
    "expired_on": "2013-08-17T23:59:00-0700",
    "updated_on": "2012-07-23T13:16:53-0700",
    "form": {
      "form_id": 183,
      "name": "Student Evaluation of Clerkship Administration",
      "active": true,
      "url": "/api/v1/forms/183"
    },
    "event": {
      "event_id": 9518,
      "category": "rotation",
      "name": "NEUROLOGY CLERKSHIP",
      "abbrev": "NEUR 701",
      "starts_on": "2012-06-18T00:00:00-0700",
      "ends_on": "2012-07-16T23:59:59-0700",
      "all_day_event": true,
      "location": {
        "site": null,
        "building": null,
        "room": null
      },
      "url": "/api/v1/events/rotation/9518"
    },
    "group": {
      "group_id": 87,
      "name": "Yr3 Neuro",
      "full_name": "---",
      "url": "/api/v1/groups/87"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/1"
  },
  {
    "evaluation_id": 112913,
    "type": "contributing",
    "status": "completed",
    "evaluator_is_anonymous": false,
    "evaluator_role": {
      "id": 3,
      "name": "attending"
    },
    "evaluator": {
      "person_id": 1098,
      "firstname": "Nephtali",
      "middlename": "R",
      "lastname": "Gomez",
      "email": null,
      "role": {
        "id": 3,
        "name": "attending"
      },
      "url": "/api/v1/people/1098"
    },
    "target_role": {
      "id": 1,
      "name": "student"
    },
    "target": {
      "person_id": 4258,
      "firstname": "Nolan",
      "middlename": "S",
      "lastname": "Maloney",
      "email": null,
      "role": {
        "id": 1,
        "name": "student"
      },
      "url": "/api/v1/people/4258"
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2014-08-18T00:00:00-0700",
    "expired_on": "2015-09-17T23:59:00-0700",
    "updated_on": "2014-12-30T08:33:29-0800",
    "form": {
      "form_id": 866,
      "name": "Year 4 Required Clerkship Director or Designee Evaluation of Student",
      "active": true,
      "url": "/api/v1/forms/866"
    },
    "event": {
      "event_id": 14016,
      "category": "rotation",
      "name": "Loma Linda University Medical Center",
      "abbrev": "LLUMC",
      "starts_on": "2012-12-17T00:00:00-0800",
      "ends_on": "2012-12-31T23:59:59-0800",
      "all_day_event": true,
      "location": {
        "site": null,
        "building": null,
        "room": null
      },
      "url": "/api/v1/events/rotation/14016"
    },
    "group": {
      "group_id": 120,
      "name": "Yr4 Surg ICU",
      "full_name": "---",
      "url": "/api/v1/groups/120"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/112913"
  },
   {
    "evaluation_id": 113341,
    "type": "contributing",
    "status": "sent",
    "evaluator_is_anonymous": false,
    "evaluator_role": {
      "id": 3,
      "name": "attending"
    },
    "evaluator": {
      "person_id": 3219,
      "firstname": "Raymond",
      "middlename": "Y",
      "lastname": "Wong",
      "email": null,
      "role": {
        "id": 3,
        "name": "attending"
      },
      "url": "/api/v1/people/3219"
    },
    "target_role": {
      "id": 1,
      "name": "student"
    },
    "target": {
      "person_id": 4106,
      "firstname": "Matthew",
      "middlename": "P",
      "lastname": "Curtis",
      "email": null,
      "role": {
        "id": 1,
        "name": "student"
      },
      "url": "/api/v1/people/4106"
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2014-08-24T00:00:00-0700",
    "expired_on": "2015-08-19T23:59:00-0700",
    "updated_on": "2014-08-24T02:37:34-0700",
    "form": {
      "form_id": 397,
      "name": "Clerkship Director Final Evaluation of Student",
      "active": true,
      "url": "/api/v1/forms/397"
    },
    "event": {
      "event_id": 13245,
      "category": "curriculum",
      "name": "MICR 547 Medical Microbiology",
      "abbrev": "MICR 547 Medical Microbiology",
      "starts_on": "2014-08-18T00:00:00-0700",
      "ends_on": "2015-05-29T00:00:00-0700",
      "all_day_event": false,
      "location": null,
      "url": "/api/v1/events/curriculum/13245"
    },
    "group": {
      "group_id": 85,
      "name": "Yr3 Med",
      "full_name": "---",
      "url": "/api/v1/groups/85"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/113341"
  },
  {
    "evaluation_id": 113342,
    "type": "contributing",
    "status": "sent",
    "evaluator_is_anonymous": false,
    "evaluator_role": {
      "id": 3,
      "name": "attending"
    },
    "evaluator": {
      "person_id": 3219,
      "firstname": "Raymond",
      "middlename": "Y",
      "lastname": "Wong",
      "email": null,
      "role": {
        "id": 3,
        "name": "attending"
      },
      "url": "/api/v1/people/3219"
    },
    "target_role": {
      "id": 1,
      "name": "student"
    },
    "target": {
      "person_id": 3879,
      "firstname": "Tabitha",
      "middlename": "D",
      "lastname": "Crane",
      "email": null,
      "role": {
        "id": 1,
        "name": "student"
      },
      "url": "/api/v1/people/3879"
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2014-08-24T00:00:00-0700",
    "expired_on": "2015-08-19T23:59:00-0700",
    "updated_on": "2014-08-24T02:37:34-0700",
    "form": {
      "form_id": 397,
      "name": "Clerkship Director Final Evaluation of Student",
      "active": true,
      "url": "/api/v1/forms/397"
    },
    "event": {
      "event_id": 13245,
      "category": "curriculum",
      "name": "MICR 547 Medical Microbiology",
      "abbrev": "MICR 547 Medical Microbiology",
      "starts_on": "2014-08-18T00:00:00-0700",
      "ends_on": "2015-05-29T00:00:00-0700",
      "all_day_event": false,
      "location": null,
      "url": "/api/v1/events/curriculum/13245"
    },
    "group": {
      "group_id": 85,
      "name": "Yr3 Med",
      "full_name": "---",
      "url": "/api/v1/groups/85"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/113342"
  },
  {
    "evaluation_id": 113088,
    "type": "contributing",
    "status": "saved",
    "evaluator_is_anonymous": false,
    "evaluator_role": {
      "id": 3,
      "name": "attending"
    },
    "evaluator": {
      "person_id": 1601,
      "firstname": "Soo",
      "middlename": "Y",
      "lastname": "Kim",
      "email": null,
      "role": {
        "id": 3,
        "name": "attending"
      },
      "url": "/api/v1/people/1601"
    },
    "target_role": {
      "id": 1,
      "name": "student"
    },
    "target": {
      "person_id": 4194,
      "firstname": "Michael",
      "middlename": "C",
      "lastname": "Nwosu",
      "email": null,
      "role": {
        "id": 1,
        "name": "student"
      },
      "url": "/api/v1/people/4194"
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2014-08-19T00:00:00-0700",
    "expired_on": "2015-08-20T23:59:00-0700",
    "updated_on": "2014-10-03T12:39:55-0700",
    "form": {
      "form_id": 1787,
      "name": "Pediatric Clerkship Director Final Evaluation of Student",
      "active": true,
      "url": "/api/v1/forms/1787"
    },
    "event": {
      "event_id": 13245,
      "category": "curriculum",
      "name": "MICR 547 Medical Microbiology",
      "abbrev": "MICR 547 Medical Microbiology",
      "starts_on": "2014-08-18T00:00:00-0700",
      "ends_on": "2015-05-29T00:00:00-0700",
      "all_day_event": false,
      "location": null,
      "url": "/api/v1/events/curriculum/13245"
    },
    "group": {
      "group_id": 91,
      "name": "Yr3 Peds",
      "full_name": "---",
      "url": "/api/v1/groups/91"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/113088"
  },
  {
    "evaluation_id": 113157,
    "type": "contributing",
    "status": "saved",
    "evaluator_is_anonymous": false,
    "evaluator_role": {
      "id": 3,
      "name": "attending"
    },
    "evaluator": {
      "person_id": 4806,
      "firstname": "Thuyvan",
      "middlename": null,
      "lastname": "Nguyen",
      "email": null,
      "role": {
        "id": 3,
        "name": "attending"
      },
      "url": "/api/v1/people/4806"
    },
    "target_role": {
      "id": 1,
      "name": "student"
    },
    "target": {
      "person_id": 4001,
      "firstname": "KeAndrea",
      "middlename": "R",
      "lastname": "Titer",
      "email": null,
      "role": {
        "id": 1,
        "name": "student"
      },
      "url": "/api/v1/people/4001"
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2014-08-20T00:00:00-0700",
    "expired_on": "2015-08-27T23:59:00-0700",
    "updated_on": "2014-11-19T11:42:25-0800",
    "form": {
      "form_id": 401,
      "name": "Mid-Rotation / Preceptor Progress Evaluation of Student",
      "active": true,
      "url": "/api/v1/forms/401"
    },
    "event": {
      "event_id": 10794,
      "category": "curriculum",
      "name": "MDCJ 528 Evidence-Based Medicine &amp; Information Science",
      "abbrev": "MDCJ 528 Evidence-Based Medicine &amp; Information Science",
      "starts_on": "2014-08-04T00:00:00-0700",
      "ends_on": "2015-06-12T00:00:00-0700",
      "all_day_event": false,
      "location": null,
      "url": "/api/v1/events/curriculum/10794"
    },
    "group": {
      "group_id": 115,
      "name": "Yr4 Med Sub-I",
      "full_name": "---",
      "url": "/api/v1/groups/115"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/113157"
  },
   {
    "evaluation_id": 348,
    "type": "contributing",
    "status": "closed",
    "evaluator_is_anonymous": true,
    "evaluator_role": {
      "id": 1,
      "name": "student"
    },
    "evaluator": {
      "person_id": null,
      "firstname": "anonymous",
      "middlename": "anonymous",
      "lastname": "anonymous",
      "email": "anonymous",
      "role": null,
      "url": null
    },
    "target_role": {
      "id": -1,
      "name": "event"
    },
    "target": {
      "event_id": 13816,
      "category": "rotation",
      "name": "Veterans Administration Hospital - Loma Linda, Ca.",
      "abbrev": "VAH - Geropsychiatry",
      "starts_on": "2012-07-09T00:00:00-0700",
      "ends_on": "2012-07-16T23:59:59-0700",
      "all_day_event": true,
      "location": {
        "site": null,
        "building": null,
        "room": null
      },
      "url": "/api/v1/events/rotation/13816",
      "block": {
        "starts_on": "2012-07-09T00:00:00-0700",
        "ends_on": "2012-07-15T23:59:59-0700",
        "id": 621,
        "active": true
      }
    },
    "head_evaluator": null,
    "completed_on": null,
    "received_on": "2012-09-11T00:00:00-0700",
    "expired_on": "2013-09-06T23:59:00-0700",
    "updated_on": "2013-09-07T05:29:50-0700",
    "form": {
      "form_id": 150,
      "name": "Student Evaluation of Clerkship Site",
      "active": true,
      "url": "/api/v1/forms/150"
    },
    "event": {
      "event_id": 13816,
      "category": "rotation",
      "name": "Veterans Administration Hospital - Loma Linda, Ca.",
      "abbrev": "VAH - Geropsychiatry",
      "starts_on": "2012-07-09T00:00:00-0700",
      "ends_on": "2012-07-16T23:59:59-0700",
      "all_day_event": true,
      "location": {
        "site": null,
        "building": null,
        "room": null
      },
      "url": "/api/v1/events/rotation/13816"
    },
    "group": {
      "group_id": 93,
      "name": "Yr3 Psych",
      "full_name": "---",
      "url": "/api/v1/groups/93"
    },
    "distributor": null,
    "url": "/api/v1/evaluations/348"
  },
];